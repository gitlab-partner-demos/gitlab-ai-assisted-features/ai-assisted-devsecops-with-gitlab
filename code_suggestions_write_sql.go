package main
type CategorySummary struct {
    Title       string
    Tasks       int
    AvgValue    float64
}
func createTables(db *sql.DB) {
    db.Exec("CREATE TABLE tasks (id INTEGER PRIMARY KEY, title TEXT, value INTEGER, category TEXT)")
}
func main() {
    db, err := sql.Open("sqlite3", "./tasks.db")
    if err !=nil {
        panic(err)
    }
    defer db.Close()
    createTables(db)
    rows, err := db.Query("SELECT * FROM tasks")
    if err!=nil {
        panic(err)
    }
    defer rows.Close()
    for rows.Next() {
        var task CategorySummary
        err := rows.Scan(&task.Title, &task.Tasks, &task.AvgValue)
        if err!=nil {
            panic(err)
        }
        fmt.Println(task)
    }
}
### Getting Started:

To present your own demo, you can use any project with a simple web application.

You need:

- Make sure you have [enabled the AI features](https://docs.gitlab.com/ee/user/ai_features.html#enable-aiml-features) in your group
- Create an issue with a description and add some comments (example below)
- Create a MR and commit some code such as adding an API call, a class,...

![](images/issue_description.png) ![](images/issue_description_2.png) 


### Intro

Starting with a poll of the audience:

On average, how much time do developers spend coding per day?

- Under 1 hour
- Between 1 and 3 hours
- Between 3 and 5 hours
- More than 5 hours


### Gitlab´s AI/ML vision

Transition into the slide deck

[GitLab AI Customer Deck - Full Deck](GitLab_AI_Customer_Deck_-_Full_Deck.pdf) - present slides 1 to 11


### Live Demo

#### I- The developer´s workflow

How Gitlab helps increase developers productivity by improving workflows at each steps of the SDLC

1. A developer is assigned to an issue: [https://gitlab.com/gitlab-partner-demos/gitlab-ai-assisted-features/ai-assisted-devsecops-with-gitlab/-/issues/1](https://gitlab.com/gitlab-partner-demos/gitlab-ai-assisted-features/ai-assisted-devsecops-with-gitlab/-/issues/1)

1. The issue contains a long description and various comments between the different team members

\>\>\>\> Issue comment summary

![](images/issue_description_3.png)

Generates a summary of the issue content. This summary feature also exists for MR and Epics.

Benefits:

- Save time
- Be quickly operational to start working on resolving the issue
- Allows all team members to be on the same page and follow the conversation

1. The developer is ready to work on the issue and opens a MR
2. Open the web IDE and start to code (can also use the VS code extension)
3. Write some code

Here is an example comment to copy inside the API resource file

_// Represents the 'homepage.title' and 'homepage.message' configuration properties injected by Quarkus_

\>\>\>\> Code Suggestions

![](images/code_suggestions.png)

When adding a comment in natural language, we receive suggestions that take the both the code and the comment into consideration to deliver tailored recommendations

This function helps automate repetitive tasks when writing code such as adding functions, integrating an API,...

Gitlab supports 13 programming languages.

This functionality is available in the web IDE as well as in the VS code extension with a plan to expand it to JetBrains IDE and MS Visual Studio

Benefits:

- Allows developers to write code more efficiently
- Gain productivity, focus and more room for innovation

1. Commit the code
2. Create a Merge Request

Show how the MR has triggered a pipeline that contains all the required build, test and deploy jobs including all the security scans.

\>\>\>\> MR Summary

![](images/mr_summary.png)

This feature is an Experiment on GitLab.com that is using Google’s Vertex service and the text-bison model. It requires the group-level third-party AI features setting to be enabled.

These summaries are automatically generated. They are available on the merge request page in the Merge request summaries dialog, the To-Do list, and in email notifications.

See how this automatically generates a comment with a summary that is visible by all in the MR

Benefits:

- Helps the MR author to communicate efficiently with the team and the future reviewer what this MR is delivering
- Facilitates collaboration between the author and the reviewer which saves time for both

1. The MR is ready for the review

\>\>\>\> Suggested reviewer

![](images/suggested_reviewers.png)

The reviewer menu shows a list of user and recommends the users that are the most relevant for the review within the context.

It leverages a project's contribution graph to generate suggestions. This data already exists within GitLab including merge request metadata, source code files, and GitLab user account metadata.

It can be used in addition to already existing tools such as MR approval rules and Code Owners.

Benefits:

- Faster and better code review since the reviewer comes with the necessary knowledge to help

#### II- The reviewer ´s workflow

1. Now looking from the point of view of another developer in the team that is added as a reviewer on a MR

[https://gitlab.com/gitlab-partner-demos/gitlab-ai-assisted-features/ai-assisted-devsecops-with-gitlab/-/merge_requests/5/diffs](https://gitlab.com/gitlab-partner-demos/gitlab-ai-assisted-features/ai-assisted-devsecops-with-gitlab/-/merge_requests/5/diffs)

A certain number of files in the project have been modified.

You can directly view the summary of committed changes made within a MR in the MR diff interface.

![](images/mr_summary_2.png)

Alternatively, you can:

1. Go to the repository in the branch linked to the MR
2. Go to the file you want to explain

[https://gitlab.com/gitlab-partner-demos/gitlab-ai-assisted-features/ai-assisted-devsecops-with-gitlab/-/blob/main/code_suggestions_write_sql.go?ref_type=heads](https://gitlab.com/gitlab-partner-demos/gitlab-ai-assisted-features/ai-assisted-devsecops-with-gitlab/-/blob/main/code_suggestions_write_sql.go?ref_type=heads)

\>\>\>\> Explain this code

![](images/explain_code.png)

Gives an explanation of what the highlighted section of code does in the context

Benefits:

- Continuous learning
- Increase team members knowledge
- Facilitate the code review process

Allows you to quickly become familiar with the code and ramp up on context for the changes, especially in the code review process.

1. Go back to the changes tab in the MR

These code changes are fine but there are no unit tests for this new API call

\>\>\>\> Generate tests in MR

Used as a template of reference when writing unit tests.

Before simply copying the code, it is still necessary to review the proposed code and adapt it if necessary.

Benefits

- Ensure optimal test coverage
- Increase code quality
- Improved both the developers and the QA teams workflows

1. The code review is completed

The developper can go back to making the changes based on the comments and adding the unit tests

#### III- Security and operations workflows

Gitlab does not only automatise processes for developers but improves every step of the SDLC, including workflows handled by security and operations teams.

1. Go to vulnerability dashboard

[https://gitlab.com/groups/gitlab-partner-demos/-/security/vulnerabilities](https://gitlab.com/groups/gitlab-partner-demos/-/security/vulnerabilities)

See the list of all vulnerabilities in the project and take actions (triage, assignment,..)

1. Filter by SAST vulnerabilities
2. Select the first vulnerability

\>\>\>\> Explain this vulnerability

![](images/explain_vulnerability.png)

Gitlab enables users to identify an effective way of correcting a vulnerability by combining basic information about the vulnerability with information from their own code.

Receive both an explanation of the vulnerability and a solution for the remediation within a single view.

Comes in addition to the 3rd party training tools already available for further learning.

1. Open an issue and assign a developer to it

Benefits:

- Increase knowledge around vulnerability remediation
- Deliver more secure code

1. Go back to project root

We have interacted with the chat at various points of these workflows. It is possible to open the chat at any time to search for information about Gitlab, particularly about the documentation, without leaving the platform

\>\>\>\> GitLab Chat

![](images/gitlab_chat.png)

Type in a question into the GitLab Chat interface

Example: is there a dashboard in Gitlab to see the DORA metrics?

Benefits:

- Avoid context switching
- Intuitive search with natural language
- Speed up information research

1. Go to Analytics dashboard
2. Go to deployment frequency

The Value Stream Dashboard provides general indications of the organization's performance.

For example, it provides a report on the average deployment frequency.

1. Toggle forecasting

\>\>\> VSA forecasting

![](images/Value_Stream_Forecasting.png)

The forecasting option visualizes trends in the development process based on historical data,

Benefits:

- Make predictions
- Identify bottlenecks early and take corrective measures

**To sum up - recap message**

- Enable developers to be more productive beyond code creation
- One workflow to unite your developers, security, and operations teams powered by AI
- From planning and code creation to testing, security and monitoring, we help you build better, more secure software faster

### Going further - The roadmap

[GitLab AI Customer Deck - Full Deck](GitLab_AI_Customer_Deck_-_Full_Deck.pdf) - present slides 23 to 29 (roadmap & ML ops)

### References

​​[GitLab Generative AI Feature Walkthrough](https://www.youtube.com/watch?v=ILJeqWoVswM)

[AI/ML powered features | GitLab](https://docs.gitlab.com/ee/user/ai_features.html)

AI features

1. Code Suggestions
2. Suggested Reviewers
3. Summarize MR Changes
4. Summarize My MR Review
5. Explain This Vulnerability
6. Generate Tests in MRs
7. Explain This Code
8. Issue Comment Summaries
9. GitLab Chat
10. Value Stream Forecasting

#### FAQ

- Timeline IntelliJ plugin (see [here](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-in-other-ides-and-editors))

- 16.2 Release Timeline

[https://gitlab.com/groups/gitlab-org/-/epics/10552](https://gitlab.com/groups/gitlab-org/-/epics/10552)

[https://gitlab.com/groups/gitlab-org/-/epics/10744](https://gitlab.com/groups/gitlab-org/-/epics/10744)

- Roadmap of features + Timelines

- Direction page: [https://about.gitlab.com/direction/modelops/ai\_assisted/](https://about.gitlab.com/direction/modelops/ai_assisted/)

- Epic backlog [https://gitlab.com/groups/gitlab-org/-/epics/688](https://gitlab.com/groups/gitlab-org/-/epics/688)

- Roadmap particularly of AI for QA (generate unit tests)

- Issue for generate unit test to give feedback and see progress: [https://gitlab.com/groups/gitlab-org/-/epics/10366](https://gitlab.com/groups/gitlab-org/-/epics/10366)

- Link for ModelOps RSS news + release news

- Sign up form to receive updates about MLOps news [https://docs.google.com/forms/d/e/1FAIpQLSdi-LuXi\_O9AoQ4Lh6tPFV8-eJlxbjTuE9oHW\_yb1Dr-vErKw/viewform](https://docs.google.com/forms/d/e/1FAIpQLSdi-LuXi_O9AoQ4Lh6tPFV8-eJlxbjTuE9oHW_yb1Dr-vErKw/viewform)

- Blog: [https://about.gitlab.com/blog/](https://about.gitlab.com/blog/)
